from home_server.models import User
from settings import Settings


class Database:
    @staticmethod
    def user_exists(username: str) -> bool:
        """Returns True if user exist, else False"""
        query = User.select().where(User.username == username)
        return len(query) > 0

    # noinspection PyTypeChecker
    @staticmethod
    def get_user(username: str) -> User:
        """Returns requested user if user exists, else None"""
        query = User.select().where(User.username == username)
        if len(query) != 0:
            return query[0]
        else:
            return None

    @staticmethod
    def add_user_to_db(username: str, password: str, pubkey: str) -> bool:
        """Adds user to the Database, True if success"""
        if not Database.user_exists(username):
            new_user = User(username=username, pubkey=pubkey)
            new_user.set_password(password)
            new_user.save()
            return True
        else:
            return False

    @staticmethod
    def verify_login(username: str, password: str) -> bool:
        """Test if the login credentials are correct"""
        user = Database.get_user(username)
        if user is None:
            return False
        else:
            return user.verify_password(password)

    @staticmethod
    def change_pw(username: str, password: str) -> bool:
        """Changes user password"""
        user = Database.get_user(username)
        if user is None:
            return False
        else:
            user.set_password(password)
            user.save()
            return True

    @staticmethod
    def remove_user(username) -> bool:
        user = Database.get_user(username)
        if user is None:
            return False
        else:
            user.delete_instance()
            return True
