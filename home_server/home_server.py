"""
The server contacted by customers, e.g. App.

:author: Thogs
:date: 28.4.18
"""

import logging

import requests
import json
from flask import abort, request, Response, jsonify
from flask_classy import FlaskView, route
from flask_httpauth import HTTPBasicAuth

from models.alias_signature import AliasSignature
from models.user_alias import UserAlias
from models.contract import Contract
from home_server.models import User
from models.charging_station import ChargingStation
from home_server.database import Database
from settings import Settings

logger = logging.getLogger("HomeServer")

auth = HTTPBasicAuth()


@auth.verify_password
def verify_pass(username, password):
    password = password.strip()
    return Database.verify_login(username, password)

class HomeServer(FlaskView):


    # TODO Brute-Force Protect
    @auth.login_required
    @route("/login", methods=["POST", "GET"])
    def login(self):
        return Settings.success_json()

    @auth.login_required
    @route("/sign_alias", methods=["POST", "GET"])
    def sign_alias(self):
        alias_signature = AliasSignature()
        values = request.get_json()
        try:
            alias_signature.from_json(json.dumps(values))
        except:
            abort(400)

        resp = requests.post("http://" + Settings.BLOCKCHAIN_SERVER_ADDRESS + "/chain/is_alias_unique",
                             json={"alias_pubkey": alias_signature.alias})
        if resp.status_code != 200:
            abort(400)

        resp = requests.post("http://" + Settings.BLOCKCHAIN_SERVER_ADDRESS + "/chain/sign_alias",
                             json=json.loads(alias_signature.to_json()))

        user_alias = UserAlias()
        user_alias.pubkey = User.select().where(User.username == auth.username())[0].pubkey
        user_alias.alias = alias_signature.alias
        user_alias.save()

        return jsonify(resp.json())

    @auth.login_required
    @route("/get_contracts", methods=["POST", "GET"])
    def get_contracts(self):
        values = request.get_json()
        if not ("start_id" in values and "count" in values):
            abort(400)

        try:
            start_id = int(values["start_id"])
            count = int(values["count"])
        except:
            abort(400)

        if count > Settings.MAX_COUNT_GET_CONTRACTS:
            abort(400)

        user = User.select().where(User.username == auth.username())[0]
        user_aliases = UserAlias.select().where(UserAlias.pubkey == user.pubkey)
        unused_aliases = [alias for alias in user_aliases if not alias.used]

        resp = requests.post("http://" + Settings.BLOCKCHAIN_SERVER_ADDRESS + "/chain/get_contracts",
                            json=[alias.alias for alias in unused_aliases])

        contracts = resp.json()
        for raw_contract in contracts:
            contract = Contract()
            contract.from_json(raw_contract)
            #contract.save()
            for alias in unused_aliases:
                if alias.alias == contract.user_signature.alias:
                    #alias.used = True
                    #alias.save()
                    pass

        """
        used_aliases = [alias for alias in user_aliases if alias.used]
        contracts = []
        for alias in used_aliases:
            contract = Contract.select().where(Contract.user_signature.alias == alias.alias
                                               and Contract.id >= start_id and Contract.id < start_id + count)
            if len(contract) > 0:
                contracts.append(contract[0])
        """

        return jsonify({"contracts" : json.dumps(contracts)})

    @auth.login_required
    @route("/get_charging_stations", methods=["POST", "GET"])
    def get_charging_stations(self):

        # TODO: Add position dependent getting

        resp = request.post("http://" + Settings.BLOCKCHAIN_SERVER_ADDRESS + "/chain/get_charging_stations",
                            json={"start_id" : len(ChargingStation.select().where())})

        new_charging_stations = resp.json()
        for i in range(len(new_charging_stations)):
            new_cs = ChargingStation()
            new_cs.from_json(new_charging_stations[i])
            new_cs.save()

        charging_stations = ChargingStation.select().where()

        return jsonify(charging_stations)

    @route("/register_user", methods=["POST", "GET"])
    def register_user(self):
        values = request.get_json()
        if not ("username" in values and "password" in values and "pubkey" in values):
            abort(400)

        password = values["password"].strip()

        Database.add_user_to_db(values["username"], password, values["pubkey"])

        return Settings.success_json()
