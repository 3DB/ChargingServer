from peewee import *
from passlib.hash import sha256_crypt

from settings import Settings

db = Settings.LOGIN_DB


class User(Model):
    username = CharField()
    pass_hash = CharField()
    pubkey = CharField()

    def set_password(self, password: str) -> None:
        """Hash Password and saves it"""
        self.pass_hash = sha256_crypt.encrypt(password)

    def verify_password(self, password: str) -> bool:
        """return """
        return sha256_crypt.verify(password, self.pass_hash)

    class Meta:
        database = db

db.create_tables([User])
