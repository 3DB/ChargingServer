from flask import Flask
import coloredlogs
import logging

from foreign_server.foreign_server import ForeignServer
from home_server.home_server import HomeServer

# Logging
LOGFORMAT = "%(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
coloredlogs.install()
logger = logging.getLogger(__name__)

app = Flask(__name__)

ForeignServer.register(app, route_base='/')
HomeServer.register(app, route_base='/')
app.run(host="0.0.0.0", port=5003)
