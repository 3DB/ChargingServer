"""
The server contacted when charging at a station.

:author: Thogs
:date: 28.4.18
"""

import json
import logging
from time import sleep, time

import requests
from flask import abort, request, jsonify
from flask_classy import FlaskView
from flask_classy import route

from crypto import Crypto
from models.contract import Contract
from models.alias_signature import AliasSignature
from settings import Settings

logger = logging.getLogger("ForeignServer")


class ForeignServer(FlaskView):
    route_base = '/'
    open_contracts = []
    running_contracts = []
    running_status = {}

    def _clean_contracts(self):
        while True:
            sleep_time = Settings.DEFAULT_SLEEP_TIME_CLEAN_CONTRACTS
            for contract_array in self.open_contracts.copy():
                contract = contract_array[0]
                if contract.start_timestamp < int(time()) + Settings.EXPIRATION_TIME_CONTRACT:
                    self.open_contracts.remove(contract_array)
                else:
                    if time() + sleep_time > contract.start_timestamp + Settings.EXPIRATION_TIME_CONTRACT:
                        sleep_time = contract.start_timestamp + Settings.EXPIRATION_TIME_CONTRACT - time()

            for c in self.running_contracts.copy():
                if int(time()) > c.end_timestamp + int(Settings.HEARTBEAT_TIME * 2.2):
                    requests.post("http://" + Settings.BLOCKCHAIN_SERVER_ADDRESS + "/chain/add_contract",
                                         json=json.loads(c.to_json()))
                    self.running_contracts.remove(c)

            sleep(sleep_time + 1)

    @route("/begin_charging", methods=["POST", "GET"])
    def begin_charging(self):
        values = request.get_json()
        if not ("station_id" in values and "token" in values and "rand_id" in values):
            abort(400)

        if not int(values["station_id"]) in Settings.VALID_STATIONS:
            abort(400)

        if len(values["token"]) != 32:
            abort(400)

        cur_rand_id = requests.get(self._get_charging_station_address(int(values["station_id"])) + "/check_rand_id")

        if cur_rand_id.json()["rand_id"] != values["rand_id"]:
            abort(403)

        #for open_contract in self.open_contracts:
        #    if open_contract[0].station_id == int(values["station_id"]):
        #        abort(409)

        contract = Contract()
        contract.price_per_kwh = Settings.PRICE_PER_KWH
        contract.additions = Settings.PRICE_ADDITIONS
        contract.station_id = int(values["station_id"])
        contract.set_start_timestamp()
        contract.set_user_hash()

        self.open_contracts.append([contract, values["token"]])

        return jsonify(json.loads(contract.to_json()))

    @route("/accept_contract", methods=["POST", "GET"])
    def accept_contract(self):
        values = request.get_json()

        if not ("alias_signature" in values and "token" in values):
            abort(400)

        token = values["token"]
        alias_signature = AliasSignature()

        try:
            alias_signature.from_json(values["alias_signature"])
        except:
            abort(400)

        contract_array = None
        for open_contract in self.open_contracts:
            if open_contract[1] == token:
                contract_array = open_contract
                break

        if contract_array is None:
            abort(428)

        contract = contract_array[0]

        if not Crypto.verify(contract.user_hash, alias_signature.contract_signature, alias_signature.alias):
            abort(400)

        if int(time()) - Settings.EXPIRATION_TIME_ALIAS > alias_signature.time_signed:
            abort(410)

        resp = requests.post("http://" + Settings.BLOCKCHAIN_SERVER_ADDRESS + "/chain/verify_alias",
                             json=alias_signature.to_json())
        if resp.status_code != 200:
            abort(400)

        contract.user_signature = alias_signature

        self.running_contracts.append(contract)
        self.open_contracts.remove(contract_array)

        requests.get(self._get_charging_station_address(contract.station_id) + "/authorise_charging")

        return Settings.success_json()

    # TODO: Add HTTP Basic Auth
    @route("/end_charging", methods=["POST", "GET"])
    def end_charging(self):
        values = request.get_json()
        if not ("station_id" in values and "charging_amount" in values):
            abort(400)

        station_id = int(values["station_id"])
        charging_amount = float(values["charging_amount"])

        contract = None
        for c in self.running_contracts:
            if c.station_id == station_id:
                contract = c
                self.running_contracts.remove(c)
                break

        self.running_status.pop(station_id, None)

        if contract == None:
            abort(400)

        contract.set_end_timestamp()
        contract.charging_amount = charging_amount
        contract.set_final_hash()

        resp = requests.post("http://" + Settings.BLOCKCHAIN_SERVER_ADDRESS + "/chain/add_contract",
                             json=json.loads(contract.to_json()))

        if resp.status_code != 200:
            abort(400)

        return Settings.success_json()

    @route("/heartbeat", methods=["POST", "GET"])
    def heartbeat(self):
        """
        values = request.get_json()
        if not ("station_id" in values and "charging_amount" in values):
            abort(400)

        station_id = int(values["station_id"])
        charging_amount = float(values["charging_amount"])

        for c in self.running_contracts:
            if c.station_id == station_id:
                c.set_end_timestamp()
                c.charging_amount = charging_amount
                c.set_final_hash()
                break
        """

        return Settings.success_json()

    @route("/station_charging_status", methods=["POST", "GET"])
    def station_charging_status(self):
        values = request.get_json()
        if not ("station_id" in values and "cur_volt" in values and "cur_amp" in values
                and "cur_power" in values and "charged_power" in values
                and "duration" in values and "start_time" in values):
            abort(400)

        station_id = int(values["station_id"])
        cur_volt = float(values["cur_volt"])
        cur_amp = float(values["cur_amp"])
        cur_power = float(values["cur_power"])
        charged_power = float(values["charged_power"])
        duration = int(values["duration"])
        start_time = int(values["start_time"])

        self.running_status[station_id] = {"cur_volt": cur_volt,
                                            "cur_amp": cur_amp,
                                            "cur_power": cur_power,
                                            "charged_power": charged_power,
                                            "duration": duration,
                                            "start_time": start_time}

        return Settings.success_json()

    @route("/user_charging_status", methods=["POST", "GET"])
    def user_charging_status(self):
        values = request.get_json()

        if not ("station_id" in values and "user_signature" in values):
            abort(400)

        station_id = int(values["station_id"])
        user_signature = values["user_signature"]

        for c in self.running_contracts:
            if c.station_id == station_id:# and c.user_signature == user_signature:
                if station_id in self.running_status:
                    return jsonify(self.running_status[station_id])

        abort(404)

    def _get_charging_station_address(self, id: int) -> str:
        #return "http://" + str(id) + "." + Settings.STATION_BASE_ADDRESS
        if id == 42:
            return "http://ancharOne:5005"
        elif id == 33:
            return "http://anchar3:5005"
        else:
            return "http://anchar2:5005"
