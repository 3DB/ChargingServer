"""
Represents a charging station with a link to advanced info.

:author: Thogs
:date: 13.6.18
"""

import json
import logging
from peewee import *

from settings import Settings

logger = logging.getLogger("ChargingStation")
db = Settings.DB

class ChargingStation(Model):
    authority_id = IntegerField()
    station_id = IntegerField()
    info_address = CharField()
    gps_latitude = FloatField()
    gps_longitude = FloatField()
    activity_state = CharField()

    def from_json(self, json_data: str):
        try:
            data = json.loads(json_data)

            self.authority_id = data["authority_id"]
            self.station_id = data["station_id"]
            self.info_address = data["info_address"]
            self.gps_latitude = data["gps_latitude"]
            self.gps_longitude = data["gps_longitude"]
            self.activity_state = data["activity_state"]

        except:
            error_msg = "Invalid JSON data - Couldn't create ChargingStation"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        json_raw = {}

        json_raw["authority_id"] = self.authority_id
        json_raw["station_id"] = self.station_id
        json_raw["gps_latitude"] = self.gps_latitude
        json_raw["gps_longitude"] = self.gps_longitude
        json_raw["info_address"] = self.info_address
        json_raw["activity_state"] = self.activity_state

        return json.dumps(json_raw, sort_keys=True)

    class Meta:
        database = db


db.create_tables([ChargingStation])
