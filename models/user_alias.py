"""
Saves the user's public key together with one of his aliases; used for tracking his charging contracts.

:author: Thogs
:date: 23.7.18
"""

import json
import logging
from peewee import *

from settings import Settings

logger = logging.getLogger("UserAlias")
db = Settings.DB


class UserAlias(Model):
    pubkey = CharField()
    alias = CharField()
    used = BooleanField(default=False)

    def to_json(self):
        data = {}

        data["pubkey"] = self.pubkey
        data["alias"] = self.alias
        data["used"] = self.used

        return json.dumps(data, sort_keys=True)

    def from_json(self, data: str):
        try:
            data = json.loads(data)
            self.alias = data["alias"]
            self.pubkey = data["pubkey"]
            self.used = data["used"]

        except:
            error_msg = "Invalid JSON data - Couldn't create UserSignature"
            logger.error(error_msg)
            raise ValueError(error_msg)

    class Meta:
        database = db

db.create_tables([UserAlias])
