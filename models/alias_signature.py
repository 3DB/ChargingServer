"""
A signature consisting of the alias and signature.

:author: Thogs
:date: 30.4.18
"""

import json
import logging
from peewee import *
from time import time

from settings import Settings
from models.signature import Signature
from crypto import Crypto

logger = logging.getLogger("AliasSignature")
db = Settings.DB


class AliasSignature(Model):
    alias = CharField()
    authority_signature = ForeignKeyField(Signature)
    contract_signature = CharField()
    time_signed = IntegerField()
    hash = CharField()

    def _create_hash(self) -> str:
        data = {}

        data["alias"] = self.alias
        data["time_signed"] = self.time_signed
        data["contract_signature"] = self.contract_signature

        return Crypto.sha256_json(data)

    def verify_hash(self) -> bool:
        return self._create_hash() == self.hash

    def set_timestamp(self):
        self.time_signed = int(time())

    def set_hash(self):
        self.hash = self._create_hash()

    def sign(self):
        self.authority_signature = Signature()
        self.authority_signature.set(Crypto.get_authority_id(), Crypto.sign(self.hash))

    def to_json(self):
        data = {}

        data["alias"] = self.alias
        data["time_signed"] = self.time_signed
        data["contract_signature"] = self.contract_signature
        data["hash"] = self.hash

        try:
            data["authority_signature"] = self.authority_signature.to_json()
        except:
            data["authority_signature"] = None

        return json.dumps(data, sort_keys=True)

    def from_json(self, data: str):
        try:
            data = json.loads(data)
            self.alias = data["alias"]
            self.time_signed = data["time_signed"]
            self.contract_signature = data["contract_signature"]
            self.hash = data["hash"]

            if not data["authority_signature"] is None:
                self.authority_signature = Signature()
                self.authority_signature.from_json(data["authority_signature"])

        except:
            error_msg = "Invalid JSON data - Couldn't create AliasSignature"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def save(self, force_insert=False, only=None):
        self.authority_signature.save()
        Model.save(self, force_insert=force_insert, only=only)

    class Meta:
        database = db

db.create_tables([AliasSignature])