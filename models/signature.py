"""
A signature consisting of the role id and signature.

:author: Thogs
:date: 30.4.18
"""

import json
import logging
from peewee import *

from settings import Settings

logger = logging.getLogger("Signature")
db = Settings.DB


class Signature(Model):
    role_id = IntegerField()
    signature = CharField()

    def set(self, role_id: int, signature: str):
        self.role_id = role_id
        self.signature = signature

    def to_json(self):
        data = {}

        data["role_id"] = self.role_id
        data["signature"] = self.signature

        return json.dumps(data)

    def from_json(self, data):
        try:
            data = json.loads(data)

            self.role_id = data["role_id"]
            self.signature = data["signature"]

        except:
            error_msg = "Invalid JSON data - Couldn't create signature"
            logger.error(error_msg)
            raise ValueError(error_msg)

    class Meta:
        database = db

db.create_tables([Signature])