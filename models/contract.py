"""
The contract between user and charging station.

:author: Thogs
:date: 28.4.18
"""

import json
import logging
from time import time
from peewee import *

from settings import Settings
from crypto import Crypto
from models.signature import Signature
from models.alias_signature import AliasSignature

logger = logging.getLogger("Contract")
db = Settings.DB


class Contract(Model):
    additions = FloatField()
    price_per_kwh = FloatField()
    station_id = IntegerField()
    start_timestamp = IntegerField()
    user_hash = CharField()
    user_signature = ForeignKeyField(AliasSignature)
    charging_amount = DoubleField()
    end_timestamp = IntegerField()
    final_hash = CharField()
    final_signature = ForeignKeyField(Signature)

    def to_json(self) -> str:
        data = {}

        data["additions"] = self.additions
        data["price_per_kwh"] = self.price_per_kwh
        data["station_id"] = self.station_id
        data["start_timestamp"] = self.start_timestamp
        data["user_hash"] = self.user_hash
        data["charging_amount"] = self.charging_amount
        data["end_timestamp"] = self.end_timestamp
        data["final_hash"] = self.final_hash

        try:
            data["final_signature"] = self.final_signature.to_json()
        except:
            data["final_signature"] = None

        try:
            data["user_signature"] = self.user_signature.to_json()
        except:
            data["user_signature"] = None

        return json.dumps(data)

    def from_json(self, json_data):
        try:
            data = json.loads(json_data)

            self.additions = data["additions"]
            self.price_per_kwh = data["price_per_kwh"]
            self.station_id = data["station_id"]
            self.start_timestamp = data["start_timestamp"]
            self.user_hash = data["user_hash"]
            self.charging_amount = data["charging_amount"]
            self.end_timestamp = data["end_timestamp"]
            self.final_hash = data["final_hash"]

            if data["user_signature"] is not None:
                self.user_signature = AliasSignature()
                self.user_signature.from_json(data["user_signature"])

            if data["final_signature"] is not None:
                self.final_signature = Signature()
                self.final_signature.from_json(data["final_signature"])

        except:
            error_msg = "Invalid JSON data - Couldn't create block"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def set_start_timestamp(self):
        self.start_timestamp = int(time())

    def set_end_timestamp(self):
        self.end_timestamp = int(time())

    def set_user_hash(self):
        data = {}

        data["additions"] = self.additions
        data["price_per_kwh"] = self.price_per_kwh
        data["station_id"] = self.station_id
        data["start_timestamp"] = self.start_timestamp

        self.user_hash = Crypto.sha256_json(data)

    def set_final_hash(self):
        data = {}

        data["user_hash"] = self.user_hash
        data["charging_amount"] = self.charging_amount
        data["end_timestamp"] = self.end_timestamp

        try:
            data["user_signature"] = self.user_signature.to_json()
        except:
            data["user_signature"] = None

        self.final_hash = Crypto.sha256_json(data)


    def save(self, force_insert=False, only=None):
        self.final_signature.save()
        self.user_signature.save()
        Model.save(self, force_insert=force_insert, only=only)

    class Meta:
        database = db

db.create_tables([Contract])
