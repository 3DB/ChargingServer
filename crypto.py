"""
Does all the crypto stuff we need, e.g. signing, verifying.

:author: Thogs
:date: 28.4.18
"""

import hashlib
from ecdsa import VerifyingKey
import ecdsa
import json
import base64


class Crypto:
    @staticmethod
    def sha256_text(text: str) -> str:
        return hashlib.sha256(text.encode("utf-8")).hexdigest()

    @staticmethod
    def sha256_json(json_data: dict) -> str:
        return Crypto.sha256_text(json.dumps(json_data, sort_keys=True))

    @staticmethod
    def verify(plain_text: str, signed_text: str, pubkey: str):
        signature = base64.standard_b64decode(signed_text)
        try:
            return Crypto.verifying_key_from_base64_der(pubkey).verify(signature, plain_text.encode("utf-8"),
                                                                   hashfunc=hashlib.sha256,
                                                                   sigdecode=ecdsa.util.sigdecode_der)
        except:
            return False

    @staticmethod
    def verifying_key_from_base64_der(base64_str: str) -> VerifyingKey:
        return VerifyingKey.from_der(base64.standard_b64decode(base64_str.encode("utf-8")))