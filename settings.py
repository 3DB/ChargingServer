"""
Settings required by the program.

:author: Thogs
:date: 28.4.18
"""

from peewee import *

from flask import jsonify

class Settings:
    PRICE_PER_KWH = 0.3
    PRICE_ADDITIONS = 0

    VALID_STATIONS = [42, 110, 1337, 33]
    STATION_BASE_ADDRESS = "example.org"

    EXPIRATION_TIME_CONTRACT = 60 * 1
    DEFAULT_SLEEP_TIME_CLEAN_CONTRACTS = EXPIRATION_TIME_CONTRACT / 2
    EXPIRATION_TIME_ALIAS = 60 * 10
    MAX_COUNT_GET_CONTRACTS = 100
    HEARTBEAT_TIME = 5 * 60  # 5 min

    BLOCKCHAIN_SERVER_ADDRESS = "jabbi-t520:5001"

    DB = SqliteDatabase("ChargingServer.db")
    LOGIN_DB = SqliteDatabase("Login.db")

    @staticmethod
    def success_json():
        return jsonify({"success": True})
